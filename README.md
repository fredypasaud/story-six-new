[![pipeline status](https://gitlab.com/fredypasaud/story-six-new/badges/master/pipeline.svg)](https://gitlab.com/fredypasaud/story-six-new/commits/master)

[![coverage report](https://gitlab.com/fredypasaud/story-six-new/badges/master/coverage.svg)](https://gitlab.com/fredypasaud/story-six-new/commits/master)

## Author
Fredy Pasaud Marolan Silitonga

## Heroku link
https://story-six-new.herokuapp.com
