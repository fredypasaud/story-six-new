from django import forms
from .models import Status
from django.forms import widgets

class StatusForms(forms.ModelForm):

    class Meta:
        model = Status
        fields = ['status']
        widgets = {
        'status' : forms.Textarea(attrs={'cols':10, 'rows': 5}),}

    def __init__(self, *args, **kwargs):
        super(StatusForms,self).__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
