from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve,reverse
from django.apps import apps
from myweb.apps import MywebConfig


from .models import Status
from .views import index
from .forms import StatusForms

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options

# Create your tests here.

class NewStorySixUnitTest(TestCase):

    def test_landing_is_true(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'index.html')
        self.assertContains(response,'Halo, apa kabar?',count=1,status_code=200,html = True)
        self.assertContains(response, '<form')
        # self.assertContains(response, 'id="submit"')

    def test_landing_use_correct_views(self):
        target = resolve('/')
        self.assertEqual(target.func, index)

    def create_status(self):
        new_status = Status.objects.create(status = "Coba Status")
        return new_status

    def test_check_status(self):
        c = self.create_status()
        self.assertTrue(isinstance(c, Status))
        self.assertTrue(c.__str__(), c.status)
        counting_all_status = Status.objects.all().count()
        self.assertEqual(counting_all_status, 1)

    def test_apps(self):
        self.assertEqual(MywebConfig.name, 'myweb')
        self.assertEqual(apps.get_app_config('myweb').name, 'myweb')

    def test_form(self):
        form_data = {
        'status' : 'ini adalah sebuah status',

        }
        form = StatusForms(data = form_data)
        self.assertTrue(form.is_valid())
        request = self.client.post('/', data = form_data)
        self.assertEqual(request.status_code, 302)

        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

class StorySixFunctionalTest(LiveServerTestCase):

    def setUp(self):
        # firefox
        super(StorySixFunctionalTest, self).setUp()
        firefox_options = Options()
        firefox_options.add_argument('--no-sandbox')
        firefox_options.add_argument('--headless')
        firefox_options.add_argument('--disable-gpu')
        self.browser = webdriver.Firefox(firefox_options = firefox_options)

    def tearDown(self):
        self.browser.quit()
        super(StorySixFunctionalTest, self).tearDown()

    def test_input_status(self):
        self.browser.get('http://127.0.0.1:8000')

        self.assertInHTML('Halo, apa kabar?', self.browser.page_source)

        status = self.browser.find_element_by_id('id_status')
        submit = self.browser.find_element_by_id('submit')

        status.send_keys("Coba Coba")
        submit.send_keys(Keys.RETURN)

        self.assertInHTML("Coba Coba", self.browser.page_source)
